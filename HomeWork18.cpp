#include <iostream>

template <typename T>
class Stack
{
private:
    T* arr;
    int size, maxSize;
public:
    Stack()
    {
        size = 0;
        maxSize = 5;
        arr = new T[maxSize];
    }
    ~Stack()
    {
        delete[] arr;
    }
    void Push(const T& t)
    {
        if (size == maxSize)
        {
            T* arr2;
            maxSize = size + 5;
            arr2 = new T[maxSize];
            for (int i = 0; i < size; i++)
                arr2[i] = arr[i];
            arr2[size] = t;
            size++;
            delete[] arr;
            arr = arr2;
        }
        else
        {
            arr[size] = t;
            size++;
        }
    }
    T Pop()
    {
        if (size == 0)
            throw std::exception();
        else
        {
            size--;
            T t = arr[size];
            arr[size] = T();
            return t;
        }
    }
};

int main()
{
    Stack<int> kek;
    for (int i = 1; i <= 6; i++)
        kek.Push(i);
    for (int i = 1; i <= 6; i++)
        std::cout << kek.Pop() << " ";
}
